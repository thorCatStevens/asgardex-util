import { AssetBNB, AssetBTC, AssetRuneB1A, AssetRuneNative, baseAmount } from '@xchainjs/xchain-util'
import {
  getBondMemo,
  getDepositMemo,
  getLeaveMemo,
  getSwapMemo,
  getSwitchMemo,
  getUnbondMemo,
  getWithdrawMemo,
} from './memo'

describe('memo', () => {
  describe('getSwapMemo', () => {
    it('returns memo to swap BNB', () => {
      expect(getSwapMemo({ asset: AssetBNB, address: 'bnb123', limit: baseAmount(1234) })).toEqual(
        'SWAP:BNB.BNB:bnb123:1234',
      )
    })
    it('returns memo for an empty address ', () => {
      expect(getSwapMemo({ asset: AssetBNB, limit: baseAmount(1234) })).toEqual('SWAP:BNB.BNB::1234')
    })
    it('returns memo w/o limit ', () => {
      expect(getSwapMemo({ asset: AssetBNB, address: 'bnb123' })).toEqual('SWAP:BNB.BNB:bnb123:')
    })
  })
  it('returns memo w/o address and w/o limit ', () => {
    expect(getSwapMemo({ asset: AssetBNB })).toEqual('SWAP:BNB.BNB::')
  })
  it('returns memo to swap RUNE', () => {
    expect(getSwapMemo({ asset: AssetRuneB1A, address: 'bnb123', limit: baseAmount(1234) })).toEqual(
      'SWAP:BNB.RUNE-B1A:bnb123:1234',
    )
  })
  it('returns memo to swap BTC', () => {
    expect(getSwapMemo({ asset: AssetBTC, address: 'btc123', limit: baseAmount(1234) })).toEqual(
      'SWAP:BTC.BTC:btc123:1234',
    )
  })

  describe('getDepositMemo', () => {
    it('returns memo to deposit BNB', () => {
      expect(getDepositMemo(AssetBNB)).toEqual('ADD:BNB.BNB:')
    })
    it('returns memo to deposit RUNE', () => {
      expect(getDepositMemo(AssetRuneB1A)).toEqual('ADD:BNB.RUNE-B1A:')
    })
    it('returns memo to deposit BTC', () => {
      expect(getDepositMemo(AssetBTC)).toEqual('ADD:BTC.BTC:')
    })
    it('returns memo to deposit BTC with cross-referenced address', () => {
      expect(getDepositMemo(AssetBTC, 'bnb123')).toEqual('ADD:BTC.BTC:bnb123')
    })
  })
  describe('getWithdrawMemo', () => {
    it('returns memo to withdraw BNB', () => {
      expect(getWithdrawMemo({ asset: AssetBNB, percent: 11 })).toEqual('WITHDRAW:BNB.BNB:1100')
    })
    it('returns memo to withdraw RUNE', () => {
      expect(getWithdrawMemo({ asset: AssetRuneNative, percent: 22 })).toEqual('WITHDRAW:THOR.RUNE:2200')
    })
    it('returns memo to withdraw BTC', () => {
      expect(getWithdrawMemo({ asset: AssetBTC, percent: 33 })).toEqual('WITHDRAW:BTC.BTC:3300')
    })
    it('returns memo to withdraw (asym) BTC', () => {
      expect(getWithdrawMemo({ asset: AssetBTC, percent: 100, targetAsset: AssetBTC })).toEqual(
        'WITHDRAW:BTC.BTC:10000:BTC.BTC',
      )
    })
    it('returns memo to withdraw (asym) RUNE', () => {
      expect(getWithdrawMemo({ asset: AssetBTC, percent: 100, targetAsset: AssetRuneNative })).toEqual(
        'WITHDRAW:BTC.BTC:10000:THOR.RUNE',
      )
    })
    it('adjusts percent to 100 if percent > 100 ', () => {
      expect(getWithdrawMemo({ asset: AssetBTC, percent: 101 })).toEqual('WITHDRAW:BTC.BTC:10000')
    })
    it('adjusts negative number of percent to 0', () => {
      expect(getWithdrawMemo({ asset: AssetBTC, percent: -10 })).toEqual('WITHDRAW:BTC.BTC:0')
    })
  })
  describe('getSwitchMemo', () => {
    it('returns memo to withdraw BNB', () => {
      expect(getSwitchMemo('tthor123')).toEqual('SWITCH:tthor123')
    })
  })

  describe('getBondMemo', () => {
    it('returns memo to bond', () => {
      expect(getBondMemo('tthor123')).toEqual('BOND:tthor123')
    })
  })

  describe('getUnbondMemo', () => {
    it('returns memo to unbond from BaseAmount units value', () => {
      expect(getUnbondMemo('tthor123', baseAmount('1000'))).toEqual('UNBOND:tthor123:1000')
    })
  })

  describe('getLeaveMemo', () => {
    it('returns memo to leave', () => {
      expect(getLeaveMemo('tthor123')).toEqual('LEAVE:tthor123')
    })
  })
})
